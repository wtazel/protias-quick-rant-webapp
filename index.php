<?php
	require_once("func.php");
?>

<?php
	$valid_rant = false;
	if(isset($_GET['id'])) {
		if(is_numeric($_GET['id'])) {
			$valid_rant = true;
		}
	}
?>

<html>
<head>
	<?php include("inc_head.php"); ?>
</head>
<body>
	<?php include("inc_logo.php"); ?>
	
	<div class="container">
		<div class="submit-rant-area">
			<textarea id="text" placeholder="<?php echo RandomPlaceholder(); ?>"></textarea> <button class="btn">Post</button>
		</div>
		<?php 
			if($valid_rant) {
				//if The rant exists it will be displayed. Comments will also be displayed.
				if(!ShowRant($_GET['id'])) {
					//The rant doesn't exist and this message will be displayed.
					echo '<p id="not-found-msg">No complains here! <a href="/">Go back</a> and look for something else.</p>';
				}
			} else {
				//The link to the rant is not valid so it will be ignored and all rants will show.
				ShowRants();
			}
		 ?>
	</div>
	
	<?php include("inc_footer.php"); ?>
</body>
</html>