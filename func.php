<?php
	$servername = "localhost";
	$username = "root";
	$password = "";
	$db = "protias";

	// Create connection
	$conn = new mysqli($servername, $username, $password, $db);

	// Check connection
	if ($conn->connect_error) {
		die("Please come back in a few minutes. We are experiencing technical difficulties (Server Error#1)");
	} 

	function RandomColor() {
		$i = mt_rand(1,2);
		$r = "";
		switch($i) {
			case 1: $r .= "#F8E0EC"; break;
			case 2: $r .= "#A9F5F2"; break;
		}
		
		return $r;
	}
	
	function BiColor($i) {
		$r = "";
		if($i % 2 == 0) $r .= "#CED8F6"; 
		else $r .= "#A9F5F2";
		
		return $r;
	}
	
	function RandomPlaceholder() {
		$i = mt_rand(0,6);
		$r = "";
		switch($i) {
			case 0: $r = "What seems to be the problem?"; break;
			case 1: $r = "Explain your problem..."; break;
			case 2: $r = "What's the matter?"; break;
			case 3: $r = "Get it off your chest..."; break;
			case 4: $r = "Why are you upset?"; break;
			case 5: $r = "Why are you angry?"; break;
			case 6: $r = "Complain here!"; break;
		}
		return $r;
	}
	
	function ShowRants() {
		global $conn;
		$res = $conn->query("SELECT rant_id, rant, IFNULL(poster_name, 'Anonymous') AS poster_name, time FROM rants ORDER BY time DESC");
		$i=0;
		while($row = $res->fetch_array()) {
			$rant = "";
			if(strlen($row["rant"]) > 400) {
				$rant = substr($row["rant"],0,320) . "...". '<i style="display: block; margin-top: 10px; font-weight: bold;">Click to read more</i>';
			} else {
				$rant = $row["rant"];
			}

			$adult = true;
			if(!$adult) $rant = adult_filter($rant);
			
			echo '
			<div class="rant-box" data-id="'.$row["rant_id"].'"style="background-color: '.BiColor($i).'">
				<p>'.$rant.'</p>
				
			</div>
			
			';
			$i++;
			//<div class="rant-box-f"> ~ '.$row["poster_name"].'</div>
		}
		
	}
	
	function InsertRant($rant) {
		global $conn;
		$text = $rant;
		$text = $conn->real_escape_string($text);
		$text = strip_tags($text);
			
		$result = $conn->query("INSERT INTO rants(rant) VALUES ('{$text}')");
		return $result;
		
	}
	
	function ShowRant($id) {
		global $conn;
		$res = $conn->query("SELECT r.rant_id, r.rant, c.comment_id as com_id, c.comment, COUNT(c.time) as CommentNum
							FROM rants r 
							LEFT JOIN comments c ON c.rant_id=r.rant_id 
							WHERE r.rant_id={$id}
							GROUP BY r.rant_id
							ORDER BY c.time DESC");
		if(!$res) return 0;
		if($res->num_rows != 1) return 0;
		while($row = $res->fetch_array()) {
			echo '
				<div class="rant-box" data-id="'.$row["rant_id"].'"style="background-color: '.BiColor($row["rant_id"]).'">
					<p>'.$row['rant'].'</p>
				</div>
				';
			echo '<div class="comments">
						<h2>Comments &bull; '.$row['CommentNum'].'</h2>';
			echo '
						<div class="comment-box" data-id="'.$row['com_id'].'" style="background-color: #E2E2E2;">
							<p>'.$row['comment'].'</p>
						</div>';
			echo '</div>';
		}
			
		return 1;
	}
	
	function GetRant($id) {
		global $conn;
		$res = $conn->query("SELECT rant_id, rant, IFNULL(poster_name, 'Anonymous') AS poster_name FROM rants WHERE rant_id={$id} LIMIT 1");
		$row = $res->fetch_array();
		return $row['rant'];
	}

?>
