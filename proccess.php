<?php
	require_once("func.php");
	
	$warning = "You shouldn't be here. Your IP has been logged.";
	if(isset($_POST['ver'])){
		if($_POST['ver'] == 1) {
			//Rant insert
			if(isset($_POST['text'])) {
				insert_rant($_POST['text']);
			} else {
				echo $warning;
			}
		} else if ($_POST['ver'] == 2) {
			//Coment insert
		} else {
			echo $warning;
		}
		
	} else if(isset($_POST['getrant'])) {
		global $conn;
		$id = $conn->real_escape_string($_POST['id']);
		if(is_numeric($id)) {
			echo GetRant($id);
		}
	} else {
		echo $warning;
	}

?>