<?php
	require_once("func.php");
?>

<html>
<head>
	<?php include("inc_head.php"); ?>
</head>
<body>
	<?php include("inc_logo.php"); ?>
	<div class="container rules-container"><h2>Rules</h2></div>
	<div class="container rules-container">
		
		<ol type="1">
			<li>Do NOT post anything illegal.</li>
			<li>Don't advertise or spam.</li>
			<li>Respect other posters and don't write negative replies.</li>
		</ol>
	</div>
	<div class="container rules-container">
		<p class="bold">Any post that doesn't follow the rules will get removed.</p><p>These are the only rules on P.R.O.T.i.A.S.</p>
		<p>Please check the <a href="/about.php">About page</a> and the <a href="/faq.php">FAQ page</a> for more information.
	</div>
	<?php include("inc_footer.php"); ?>
</body>
</html>